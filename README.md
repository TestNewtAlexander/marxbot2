How to setup:

Marxbot2 uses a .env file to store the token, client_id and guild_id.

To get these values you must setup the following:

- Create an application in the Discord Developer Portal https://discord.com/developers/applications
    - This provides the client_id in the OAuth2 General page
    - This also provides the token on the Bot page
- You can get the guild_id from any server you wish to use this bot on by copying the server id from Discord

Your final .env file should look like this:

> token="ABC123DEF456GHI789JKL012MNO345PQR678STU"

> client_id="1357924680"

> guild_id="1234567890"

It should be stored in the root directory.

When running this application you to run both the deployment.js file and app.js file.

deployment.js sets up all the application commands and is needed for any of the slash commands to work correctly
app.js is the main bot

Additionally for the RSS parser to work you will need to update the line:
>   const podcastChannel = client.channels.cache.get(Channels.leftymedia);

in the file channels.ts there is a list of channel ids. Add the channel id where you want the RSS feed posted and change the parameter in the above line.
