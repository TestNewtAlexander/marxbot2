import { Cancelled, CancelledObject } from "../types/cancelled";

export class Cancel {
  static cancelled(name?: string): any {
    const Store = require("data-store");
    const storage = new Store({ path: "./storage/cancelled.json" });
    const cancelled: Cancelled = storage.get();
    let outputString: string[] = [];
    if (name) {
      for (const key in cancelled) {
        if (key.toLowerCase() === name.toLowerCase()) {
          outputString.push(
            `${key} has been cancelled ${cancelled[key].cancellations} time${
              cancelled[key].cancellations != 1 ? `s` : ""
            } ${cancelled[key].reason ? `for:` : ``}`
          );
          for (const reason of cancelled[key].reason) {
            console.log(reason);
            outputString.push(`${reason}`);
          }
        }
      }
      if (outputString.length == 0) {
        return `${name} has never been cancelled`;
      }
    } else {
      for (const key in cancelled) {
        outputString.push(`${key}: ${cancelled[key].cancellations}`);
      }
    }

    return outputString.join(`\n`);
  }
  static uncancel(name: string) {
    const Store = require("data-store");
    const storage = new Store({ path: "./storage/cancelled.json" });
    const cancelled: Cancelled = storage.get();

    if (name == "@everyone" || name == "@here") return `Naughty naughty`;

    for (const key in cancelled) {
      if (key.toLowerCase() === name.toLowerCase()) {
        const nameValues = cancelled[key];
        nameValues.cancellations = nameValues.cancellations - 1;
        storage.set(cancelled);
        return `${name} has been uncancelled`;
      }
    }

    return `${name} is a pure soul and has never been cancelled`;
  }

  static cancel(name: string, reason?: string) {
    const Store = require("data-store");
    const storage = new Store({ path: "./storage/cancelled.json" });
    const cancelled: Cancelled = storage.get();

    if (name == "@everyone" || name == "@here") return `Naughty naughty`;

    for (const key in cancelled) {
      if (key.toLowerCase() === name.toLowerCase()) {
        const nameValues = cancelled[key];
        nameValues.cancellations = nameValues.cancellations + 1;
        if (reason) nameValues.reason.push(reason);
        storage.set(cancelled);
        return `${name} has been cancelled ${
          reason ? `for ${reason} ` : ""
        }they have been cancelled ${nameValues.cancellations} time${
          nameValues.cancellations != 1 ? `s` : ""
        }`;
      }
    }

    const newCancel: CancelledObject = {
      reason: reason ? [reason] : [],
      cancellations: 1,
    };

    cancelled[name] = newCancel;

    storage.set(cancelled);

    return `${name} has been cancelled ${
      reason ? `for ${reason} ` : ""
    }they have been cancelled 1 time`;
  }
}
