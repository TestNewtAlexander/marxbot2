import { GuildMember, Message } from "discord.js";
import { CannedResponses } from "../types/cannedResponses";

export class Responses {
  static cannedCompliments(user: GuildMember) {
    const Store = require("data-store");
    const storage = new Store({ path: "./storage/compliments.json" });
    const compliments = storage.get("responses");
    const response =
      compliments[Math.floor(Math.random() * compliments.length)];
    return response.replace(
      "{0}",
      user.nickname ? user.nickname : user.user.username
    );
  }

  static quotes() {
    const Store = require("data-store");
    const storage = new Store({ path: "./storage/quotes.json" });
    const quotes = storage.get("quotes");
    return quotes[Math.floor(Math.random() * quotes.length)];
  }

  static async cannedResponse(receivedMessage: Message) {
    const Store = require("data-store");
    const storage = new Store({
      path: "./storage/cannedResponses.json",
    });

    const responses: CannedResponses[] = storage.get();

    if (receivedMessage.author.bot) return; // Don't respond to bots
    for (const key in responses) {
      if (new RegExp(`\\b${key}\\b`, "i").test(receivedMessage.content)) {
        console.log(new Date());
        console.log("canned response triggered");
        const value = responses[key];
        return value;
      }
    }
    return null;
  }
}
