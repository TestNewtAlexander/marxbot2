import { Episode } from "../types/episodes";
import { Podcast } from "../types/podcasts";

export class RSSHandler {
  //RSS Feed Handler
  static async rssCheck() {
    //RSS feed
    const Turndown = require("turndown");
    let Parser = require("rss-parser");
    let parser = new Parser();

    const Store = require("data-store");
    const storage = new Store({ path: "./storage/podcasts.json" });
    const podcasts: Podcast[] = storage.get("podcasts");
    let newPodcasts: string[] = [];

    for (const podcast of podcasts) {
      //For each podcast
      let feed;
      try {
        feed = await parser.parseURL(podcast.rssFeedURL); // Get the rss feed
      } catch (_) {
        // RSS feed no longer exists
        console.log("RSS feed inaccessible: " + podcast.name);
        return newPodcasts;
      }

      // find new episodes
      var newEpisodes: Episode[] = feed.items.filter(function (
        episode: Episode
      ) {
        return new Date(episode.pubDate) > new Date(podcast.lastPublishedDate);
      });

      newEpisodes.sort(
        (a, b) => new Date(b.pubDate).getTime() - new Date(a.pubDate).getTime()
      );
      // limit number of episodes to post to 3
      if (newEpisodes.length > 3) {
        newEpisodes = newEpisodes.slice(0, 3);
      }

      newEpisodes.sort(
        (a, b) => new Date(a.pubDate).getTime() - new Date(b.pubDate).getTime()
      );

      for (const episodeItem of newEpisodes) {
        console.log("New episode on: " + episodeItem.pubDate.toString());

        var messageStart =
          `:rotating_light: Heya <@&` +
          podcast.roleID +
          `> \! :rotating_light: \n`;
        var messageName = ``;

        if (podcast.emoji) {
          // Emojis
          messageName = `:rotating_light: New ${podcast.emoji} ${podcast.name} ${podcast.emoji} episode! :rotating_light:`;
        } else {
          // No emojis
          messageName = `:rotating_light: New ${podcast.name} episode! :rotating_light:`;
        }

        var description = "";

        var turndownService = new Turndown();
        // Check if either a subtitle or summary exist
        // parse summary to markdown in case there's html
        if (episodeItem.itunes.subtitle) {
          description = turndownService
            .turndown(episodeItem.itunes.subtitle)
            .split("\n")[0];
        } else if (episodeItem.itunes.summary) {
          description = turndownService
            .turndown(episodeItem.itunes.summary)
            .split("\n")[0];
        }

        var messsageDescription = ` \nFind it here: ${episodeItem.link}\n${description}`;

        //Increment the episode number and save to avoid reposts
        podcast.lastPublishedDate = episodeItem.pubDate;
        console.log("Updating podcasts.json");
        storage.set("podcasts", podcasts);
        newPodcasts.push(messageStart + messageName + messsageDescription);
      }
    }
    return newPodcasts;
  }
}
