import { Timers } from "../types/timers";

export class TimerFunctions {
  static resetTimer(timer: any): string {
    const Store = require("data-store");
    const storage = new Store({ path: "./storage/timers.json" });
    let timers: Timers = storage.get("timers");
    let previous = timers[timer].time;
    let currentTime = Date.now();
    storage.set("timers." + timer + ".time", currentTime);
    return `It's been ${TimerFunctions.formatDuration(
      currentTime - previous
    )} since anyone noticed ${timers[timer].message}`;
  }

  private static formatDuration(duration: number): string {
    const weeks = Math.floor(duration / (1000 * 60 * 60 * 24 * 7));
    duration -= weeks * 1000 * 60 * 60 * 24 * 7;
    const days = Math.floor(duration / (1000 * 60 * 60 * 24));
    duration -= days * 1000 * 60 * 60 * 24;
    const hours = Math.floor(duration / (1000 * 60 * 60));
    duration -= hours * 1000 * 60 * 60;
    const minutes = Math.floor(duration / (1000 * 60));
    const seconds = Math.floor(duration / 1000) % 60;

    let output = "";
    if (weeks > 0) {
      output += `${weeks} week${weeks === 1 ? "" : "s"} `;
    }
    if (days > 0) {
      output += `${days} day${days === 1 ? "" : "s"} `;
    }
    if (hours > 0) {
      output += `${hours} hour${hours === 1 ? "" : "s"} `;
    }
    if (minutes > 0) {
      output += `${minutes} minute${minutes === 1 ? "" : "s"} `;
    }
    if (seconds > 0) {
      output += `and ${seconds} second${seconds === 1 ? "" : "s"}`;
    }
    return output.trim();
  }

  static howWeDoin(): string {
    const Store = require("data-store");
    const storage = new Store({ path: "./storage/timers.json" });
    let timers: Timers = storage.get("timers");
    return Object.entries(timers)
      .map(([key, value]) => {
        const duration = TimerFunctions.formatDuration(Date.now() - value.time);
        return `${duration} since anyone noticed ${value.message}`;
      })
      .join("\n");
  }
}
