import { ChatInputCommandInteraction } from "discord.js";

export class idHandler {
  static async replaceIDwithUser(
    id: string,
    interaction: ChatInputCommandInteraction
  ) {
    let outputString = id;
    const regex = /<@&?[0-9]*>/g;
    let match;

    // Use a loop to process each match asynchronously
    while ((match = regex.exec(id)) !== null) {
      const replacement = await idHandler.replaceMentionTag(
        match[0],
        interaction
      );
      outputString = outputString.replace(match[0], replacement);
    }

    return outputString;
  }

  // Define the async function to run on each match
  static async replaceMentionTag(
    id: string,
    interaction: ChatInputCommandInteraction
  ) {
    try {
      const GuildMember = await interaction.guild?.members.fetch(
        id.substring(2, id.length - 1)
      );
      id = GuildMember!.user.username;
      console.log(id);
    } catch {
      try {
        const Role = await interaction.guild?.roles.fetch(
          id.substring(3, id.length - 1)
        );
        id = Role!.name;
      } catch {
        return id;
      }
    }

    // You can add your own logic here to process the mention tag and return the replacement string
    return id;
  }
}
