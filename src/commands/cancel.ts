import { ChatInputCommandInteraction, SlashCommandBuilder } from "discord.js";
import { Cancel } from "../functions/cancel";
import { idHandler } from "../functions/idHandler";

module.exports = {
  data: new SlashCommandBuilder()
    .setName("cancel")
    .setDescription("Cancels someone")
    .addStringOption((option) =>
      option.setName("name").setDescription("Name").setRequired(true)
    )
    .addStringOption((option) =>
      option.setName("reason").setDescription("Reason for cancelling")
    ),
  async execute(interaction: ChatInputCommandInteraction) {
    let name: string = interaction.options.getString("name")!;
    name = await idHandler.replaceIDwithUser(name, interaction);
    let reason: string = interaction.options.getString("reason") || "";
    reason = await idHandler.replaceIDwithUser(reason, interaction);
    await interaction.reply(Cancel.cancel(name, reason));
  },
};
