import { SlashCommandBuilder } from "discord.js";
import { TimerFunctions } from "../functions/timerFunctions";

module.exports = {
  data: new SlashCommandBuilder()
    .setName("capitalism")
    .setDescription("Capitalism is at it again"),
  async execute(interaction: any) {
    await interaction.reply(TimerFunctions.resetTimer("capitalism"));
  },
};
