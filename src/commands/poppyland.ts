import { SlashCommandBuilder } from "discord.js";
import { TimerFunctions } from "../functions/timerFunctions";

module.exports = {
  data: new SlashCommandBuilder()
    .setName("poppyland")
    .setDescription("Someone noticed England doing something"),
  async execute(interaction: any) {
    await interaction.reply(TimerFunctions.resetTimer("poppyland"));
  },
};
