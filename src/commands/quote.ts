import { SlashCommandBuilder } from "discord.js";
import { Responses } from "../functions/responses";

module.exports = {
  data: new SlashCommandBuilder()
    .setName("quote")
    .setDescription("Get a random quote from Marx"),
  async execute(interaction: any) {
    await interaction.reply(Responses.quotes());
  },
};
