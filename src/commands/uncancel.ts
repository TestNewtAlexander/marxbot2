import { ChatInputCommandInteraction, SlashCommandBuilder } from "discord.js";
import { Cancel } from "../functions/cancel";
import { idHandler } from "../functions/idHandler";

module.exports = {
  data: new SlashCommandBuilder()
    .setName("uncancel")
    .setDescription("Uncancels someone")
    .addStringOption((option) => option.setName("name").setDescription("Name")),
  async execute(interaction: ChatInputCommandInteraction) {
    let name: string = interaction.options.getString("name")!;
    name = await idHandler.replaceIDwithUser(name, interaction);
    await interaction.reply(Cancel.uncancel(name));
  },
};
