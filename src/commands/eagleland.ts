import { SlashCommandBuilder } from "discord.js";
import { TimerFunctions } from "../functions/timerFunctions";

module.exports = {
  data: new SlashCommandBuilder()
    .setName("eagleland")
    .setDescription("Someone noticed Eagleland doing something"),
  async execute(interaction: any) {
    await interaction.reply(TimerFunctions.resetTimer("eagleland"));
  },
};
