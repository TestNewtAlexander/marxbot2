import { CommandInteraction, SlashCommandBuilder } from "discord.js";
import { Responses } from "../functions/responses";

module.exports = {
  data: new SlashCommandBuilder()
    .setName("compliment")
    .setDescription("Support your fellow comrades")
    .addUserOption((option) =>
      option.setName("user").setDescription("User").setRequired(true)
    ),
  async execute(interaction: CommandInteraction) {
    const user: any = interaction.options.get("user")!.member!;
    await interaction.reply(Responses.cannedCompliments(user));
  },
};
