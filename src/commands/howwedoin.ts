import { SlashCommandBuilder } from "discord.js";
import { TimerFunctions } from "../functions/timerFunctions";

module.exports = {
  data: new SlashCommandBuilder()
    .setName("howwedoin")
    .setDescription(
      "Checks the timers to see how long it has been since some nonsense"
    ),
  async execute(interaction: any) {
    await interaction.reply(TimerFunctions.howWeDoin());
  },
};
