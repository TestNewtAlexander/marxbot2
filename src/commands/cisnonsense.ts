import { SlashCommandBuilder } from "discord.js";
import { TimerFunctions } from "../functions/timerFunctions";

module.exports = {
  data: new SlashCommandBuilder()
    .setName("cisnonsense")
    .setDescription("Someone noticed some cis nonsense"),
  async execute(interaction: any) {
    await interaction.reply(TimerFunctions.resetTimer("cisnonsense"));
  },
};
