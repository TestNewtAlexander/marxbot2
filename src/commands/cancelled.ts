import { SlashCommandBuilder } from "discord.js";
import { Cancel } from "../functions/cancel";
import { idHandler } from "../functions/idHandler";

module.exports = {
  data: new SlashCommandBuilder()
    .setName("cancelled")
    .setDescription("Shows the list of cancellations")
    .addStringOption((option) =>
      option.setName("name").setDescription("User details")
    ),
  async execute(interaction: any) {
    let name: string = interaction.options.getString("name");
    name = await idHandler.replaceIDwithUser(name, interaction);
    await interaction.reply(Cancel.cancelled(name));
  },
};
