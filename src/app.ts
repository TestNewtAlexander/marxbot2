import fs from "node:fs";
import path from "node:path";
import {
  Client,
  Collection,
  Events,
  TextChannel,
  IntentsBitField,
} from "discord.js";
import { Responses } from "./functions/responses";
import { RSSHandler } from "./functions/rssHandler";

//@ts-ignore
import dotenv from "dotenv";
import { Channels } from "./types/channels";

dotenv.config();

const token = process.env.token;

const client = new Client({
  intents: [
    IntentsBitField.Flags.Guilds,
    IntentsBitField.Flags.GuildMessages,
    IntentsBitField.Flags.MessageContent,
  ],
});

const commands = new Collection<string, any>();
const commandsPath = path.join(__dirname, "commands");
const commandFiles = fs
  .readdirSync(commandsPath)
  .filter((file: string) => file.endsWith(".js"));

for (const file of commandFiles) {
  const filePath = path.join(commandsPath, file);
  const command = require(filePath);
  commands.set(command.data.name, command);
}

client.once(Events.ClientReady, () => {
  console.log("Ready!");
});

client.on(Events.InteractionCreate, async (interaction) => {
  if (!interaction.isChatInputCommand()) return;

  const command = commands.get(interaction.commandName);

  if (!command) return;

  try {
    await command.execute(interaction);
  } catch (error) {
    console.error(error);
    await interaction.reply({
      content: "There was an error while executing this command!",
      ephemeral: true,
    });
  }
});

client.on(Events.ClientReady, async () => {
  const podcastChannel = client.channels.cache.get(Channels.leftymedia);
  const newEpisodes = await RSSHandler.rssCheck();
  newEpisodes.forEach((episode: string) => {
    (<TextChannel>podcastChannel)!.send(episode);
  });
  setInterval(async () => {
    const newEpisodes = await RSSHandler.rssCheck();
    newEpisodes.forEach((episode: string) => {
      (<TextChannel>podcastChannel)!.send(episode);
      setTimeout(() => {}, 1000);
    });
  }, 600000);
});

//Canned message responses
client.on(Events.MessageCreate, async (receivedMessage) => {
  // Prevent bot from responding to its own messages
  if (receivedMessage.author.id === client.user?.id) {
    return;
  }

  // Check messages for canned phrases
  const canned = await Responses.cannedResponse(receivedMessage);
  if (canned) {
    (<TextChannel>receivedMessage.channel).send(canned);
    return;
  }

  // Check if the bot's user was tagged in the message
  if (receivedMessage.content.includes(client.user?.id!)) {
    // Send acknowledgement message

    (<TextChannel>receivedMessage.channel).send(Responses.quotes());
    return;
  }
});

client.login(token);
