export interface Episode {
  title: string;
  link: string;
  guid: string;
  pubDate: string;
  author: string;
  enclosure: string;
  itunes: ITunes;
  description: string;
  content: Content;
  fireside: Fireside;
  podcast: Podcast;
}

interface ITunes {
  episodeType: string;
  author: string;
  subtitle: string;
  duration: string;
  explicit: string;
  image: string;
  summary: string;
}

interface Content {
  encoded: string;
}

interface Fireside {
  playerURL: string;
  playerEmbedCode: string;
}

interface Podcast {
  person: string;
}
