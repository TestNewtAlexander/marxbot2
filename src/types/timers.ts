export interface Timers {
  [key: string]: Timer;
}

export interface Timer {
  time: number;
  message: string;
}
