export interface CannedResponses {
  [key: string]: string;
}
