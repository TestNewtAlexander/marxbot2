export interface Podcast {
  name: string;
  rssFeedURL: string;
  roleID: string;
  emoji: string;
  lastPublishedDate: string;
}
