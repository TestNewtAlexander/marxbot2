export interface configData {
  token: string;
  client_id: string;
  guild_id: string;
}
