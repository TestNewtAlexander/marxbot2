export interface Cancelled {
  [key: string]: CancelledObject;
}

export interface CancelledObject {
  reason: string[];
  cancellations: number;
}
